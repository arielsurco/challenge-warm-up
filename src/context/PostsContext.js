import { createContext, useEffect, useState } from "react";
import { LSGetItem } from "../helpers/localStorage";
import { getData } from "../helpers/request";
import { useLocalStorage } from "../hooks/useLocalStorage";

const PostsContext = createContext();

const itsIn = (arr, item, prop = null) => {
    return !!prop ? 
        arr.some(otherItem => item[`${prop}`] === otherItem[`${prop}`])
        : arr.some(otherItem => item === otherItem);
}

const PostsProvider = ({children}) => {
    const LSDeletedPosts = LSGetItem('deletedPosts');
    const LSEditedPosts = LSGetItem('editedPosts');
    const LSAddedPosts = LSGetItem('addedPosts');
    const [deletedPosts, setDeletedPosts] = useLocalStorage('deletedPosts', LSDeletedPosts || []);
    const [editedPosts, setEditedPosts] = useLocalStorage('editedPosts', LSEditedPosts || []);
    const [addedPosts, setAddedPosts] = useLocalStorage('addedPosts', LSAddedPosts || []);
    const [posts, setPosts] = useState([]);
    useEffect(() => {
        const APIurl = process.env.REACT_APP_BASE_URL_DATA_API;
        const getPosts = async () => {
            await getData(`${APIurl}/posts`)
                .then( data => {
                    const postsWithoutDeleted = data.filter(post => !itsIn(deletedPosts, post, 'id'))
                    const newPosts = postsWithoutDeleted.map(post => {
                        return itsIn(editedPosts, post, 'id') ? 
                            editedPosts.find(editedPost => post.id === editedPost.id)
                            : post;
                    })
                    setPosts([...newPosts, ...addedPosts]);
                } );
        }
        getPosts();
        //eslint-disable-next-line
    }, [])

    const addPost = data => {
        const newPost = {
            userId: 1,
            id: posts[posts.length - 1].id + 1,
            title: data.title,
            body: data.body
        }
        setPosts([...posts, newPost]);
        setAddedPosts([...addedPosts, newPost]);
    }

    const editPost = (id, data) => {
        const currentPost = searchPost(id);
        const newPosts = [...posts];
        const index = newPosts.findIndex(post => post.id === id);
        const newPost = {...currentPost, ...data};
        newPosts[index] = newPost;
        setPosts(newPosts);
        setEditedPosts([...editedPosts, newPost]);
    }

    const removePost = (postId) => {
        const toDelete = searchPost(postId);
        setPosts( posts.filter( post => post !== toDelete) );
        if(!!toDelete)
            setDeletedPosts([...deletedPosts, toDelete]);
    }

    const searchPost = postId => {
        return posts.find(post => post.id === postId);
    }

    return (
        <PostsContext.Provider value={{posts, addPost, editPost, removePost, searchPost}}>
            {children}
        </PostsContext.Provider>
    )
}

export { PostsContext, PostsProvider };