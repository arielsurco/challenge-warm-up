import { Form } from '../../components/Form';
import { InputGroup } from '../../components/Form/inputGroup';
import { InputItem } from '../../components/Form/InputItem';
import { useFormik } from 'formik';
import { validate } from '../../helpers/loginValidation';
import { useUserState } from '../../hooks/useUserState';
import './Login.css';

const Login = () => {
    const { login } = useUserState();
    const formik = useFormik({
        initialValues: {
            email: '',
            password: ''
        },
        validate,
        onSubmit: values => {
            login(values)
        }
    })

    return (
        <div className="form-container">
            <Form onSubmit={formik.handleSubmit} className="login-form">
                <h1 className="login-form__title">Welcome!</h1>
                <p className="login-form__message">Log in to continue to blog</p>
                <InputGroup>
                    <InputItem name="email" type="email" label="Email adress" 
                    value={formik.values.email} 
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    error={formik.touched.email && formik.errors.email}/>
                    <InputItem name="password" type="password" label="Password"
                    value={formik.values.password}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    error={formik.touched.password && formik.errors.password}/>
                </InputGroup>
                <button type="submit" className="login-form__btn">Login</button>
            </Form>
        </div>
    )
}

export { Login }