import { useContext } from 'react';
import { Post } from '../../components/Post'
import { PostsContext } from '../../context/PostsContext';
import './Home.css';

const Home = () => {
    const { posts } = useContext(PostsContext);
    return (
        <section className="posts-container row row-cols-xs-1 row-cols-sm-3 row-cols-lg-4 row-cols-xxl-5 gap-4">
            {posts.map(post => (
                <Post className="entry-preview flex-fill" id={post.id} preview key={post.title}>
                    <h1>{post.title}</h1>
                </Post>
            ))}
        </section>
    )
}

export { Home }