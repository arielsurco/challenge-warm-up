import { useEffect, useState } from 'react';
import { useParams } from 'react-router';
import { Post } from '../../components/Post';
import { getData } from '../../helpers/request';
import './Detail.css';

const Detail = () => {
    const [post, setPost] = useState(null);
    const [isLoading, setIsLoading] = useState(true);
    const { id } = useParams();
    useEffect(() => {
        const APIurl = process.env.REACT_APP_BASE_URL_DATA_API;
        const loadPost = async () => {
            try {
                getData(`${APIurl}/posts/${id}`)
                    .then(data => { 
                        setPost(data); 
                        setIsLoading(false);
                    });
            } catch(error) {
                console.error(new Error(error));
            }
        }
        loadPost();
    }, [id])
    return isLoading ? <p>Cargando</p> :
        <section className="detail-container p-3">
            {post ? 
            <Post className="detail" id={post.id}>
                <h1 className="detail__title">{post.title}</h1>
                <p className="fs-2">{post.body}</p>
            </Post> :
            <p>Not found</p>
            }
        </section>
}

export { Detail };