import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { PrivateRoute } from '../../components/PrivateRoute/'
import { Home } from '../Home';
import { Login } from '../Login';
import { Detail } from '../Detail';
import { PostsProvider } from '../../context/PostsContext';
import { Layout } from '../../components/Layout';
import { Error404 } from '../Error404';
import './App.css';
import { Create } from '../Create';

const App = () => {
  return (
        <PostsProvider>
        <Router>
          <Layout>
            <Switch>
              <Route exact path='/login' component={Login}/>
              <PrivateRoute exact path='/create' component={Create}/>
              <PrivateRoute exact path='/posts/:id' component={Detail}/>
              <PrivateRoute exact path='/' component={Home}/>
              <Route component={Error404}/>
            </Switch>
            </Layout>
        </Router>    
    </PostsProvider>
  )
}

export default App;
