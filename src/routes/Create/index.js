import { useFormik } from "formik"
import { useContext } from "react";
import { Form } from "../../components/Form"
import { InputGroup } from "../../components/Form/inputGroup";
import { InputItem } from "../../components/Form/InputItem";
import { PostsContext } from "../../context/PostsContext";
import './Create.css';

const validate = values => {
    const errors = {};
    if(!values.title || values.title.length === 0)
        errors.title = 'Required';

    if(!values.body || values.body.length === 0 )
        errors.body = 'Required';
    
    return errors;
}

const Create = () => {
    const { addPost } = useContext(PostsContext);
    const formik = useFormik({
        initialValues: {
            email: '',
            password: ''
        },
        validate,
        onSubmit: values => {
            addPost(values);
            window.location.pathname = '/';
        }
    })
    return (
        <div className="form-container">
            <Form onSubmit={formik.handleSubmit} className="login-form">
                <InputGroup>
                    <InputItem name="title" type="text" label="Title" 
                    value={formik.values.title} 
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    error={formik.touched.title && formik.errors.title}/>
                    <InputItem name="body" type="text" label="Body" placeholder="Write here"
                    value={formik.values.body}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    error={formik.touched.body && formik.errors.body}/>
                </InputGroup>
                <button type="submit" className="login-form__btn">Create</button>
            </Form>
        </div>
    )
}

export { Create }