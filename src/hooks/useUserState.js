import { postData } from "../helpers/request";
import { useLocalStorage } from "./useLocalStorage"

const useUserState = () => {
    const LSSession = localStorage.getItem('session');
    const initialSession = LSSession ? JSON.parse(LSSession) : {token: '', isAuth: false};
    const [session, saveSession] = useLocalStorage('session', initialSession);
    const tokenAPI = process.env.REACT_APP_TOKEN_API;
    
    const login = async (data) => {
        try {
            await postData(tokenAPI, data)
            .then( res => {
                saveSession({
                    token: res.token,
                    isAuth: true
                });
                window.location.pathname='/';
            });
        } catch(error) {
            console.error(new Error(error));
        }
    }

    const logout = () => {
        saveSession(initialSession);
    }

    return {session, login, logout}
}

export { useUserState };