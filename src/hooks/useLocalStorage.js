import { useEffect, useState } from 'react';

const useLocalStorage = (itemName, initialValue) => {
    const [item, setItem] = useState(initialValue)
    useEffect(() => {
        const LSItem = localStorage.getItem(itemName);
        let parsedItem;
        if(!LSItem) {
            localStorage.setItem(itemName, JSON.stringify(initialValue));
            parsedItem = initialValue;
        } else {
            parsedItem = JSON.parse(LSItem);
        }
        setItem(parsedItem);
        //eslint-disable-next-line
    }, [])

    const saveItem = (newItem) => {
        const stringifiedItem = JSON.stringify(newItem);
        localStorage.setItem(itemName, stringifiedItem);
        setItem(newItem);
    }
    return [item, saveItem];
}

export { useLocalStorage }