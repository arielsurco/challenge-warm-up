const LSGetItem = (itemName) => JSON.parse(localStorage.getItem(itemName));

const LSSetItem = (itemName, item) => {
    localStorage.setItem(itemName, JSON.stringify(item));
}

export {LSGetItem, LSSetItem};