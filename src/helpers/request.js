import axios from "axios"

const handleError = err => console.error(new Error(err));

const getData = async (url) => {
    try {
        return await axios.get(url).then( response => response.status === 200 && response.data);
    } catch(error) {
        handleError(error)
    }
}

const postData = async (url, element) => {
    try {
        return await axios.post(url, element).then( response => response.status === 200 && response.data);
    } catch(error) {
        handleError(error);
    }
}

export { getData, postData }