import { Route, Redirect } from 'react-router-dom'
import { useUserState } from '../../hooks/useUserState';
import { Layout } from '../Layout';

const PrivateRoute = (props) => {
    const { session } = useUserState();
    return session.isAuth ? <Route {...props}/>
    : <Redirect to="/login"/>
}

export { PrivateRoute };