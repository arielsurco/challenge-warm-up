import { Fragment, useContext } from 'react'
import { Link } from 'react-router-dom';
import { PostsContext } from '../../context/PostsContext';
import './Post.css';

const Post = ({children, className, preview, id}) => {
    const {removePost} = useContext(PostsContext);
    const userIsAuthor = true;
    return (
        <div className={`entry-container shadow d-flex flex-column justify-content-between ${className || ''}`}>
            <div className="entry-container__body">
                {children}
            </div>
            <div className="mt-3 gap-4 d-flex justify-content-md-start">
                {preview && <Link className="btn btn-primary fs-3" to={`/posts/${id}`}>Detail</Link>}
                {userIsAuthor && 
                    <Fragment>
                        <button className="btn btn-secondary fs-3">Edit</button>
                        <button onClick={() => removePost(id)} className="btn btn-danger fs-3">Delete</button>
                    </Fragment>
                }
            </div>
        </div>
    )
}

export { Post }