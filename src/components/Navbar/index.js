import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import { useUserState } from '../../hooks/useUserState';
import './Navbar.css';

const Navbar = () => {
    const { session, logout } = useUserState();

    return (
    <nav className="navbar navbar-light navbar-expand-md bg-light fixed-top shadow p-3 mb-5 bg-white rounded">
        <div className="container-fluid">
            <Link className="navbar-brand logo" to="/">
                Blog
            </Link>
            <button type="button" className="navbar-toggler" data-bs-toggle="collapse" data-bs-target="#navMenu" aria-controls="navMenu">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse navMenu" id="navMenu">
                <ul className="navbar-nav ms-3">
                    {session.isAuth ? (
                    <Fragment>
                        <li className="nav-item" >
                            <Link className="nav-link" to="/create">Create</Link>
                        </li>
                        <li className="nav-item" onClick={logout}>
                            <Link className="nav-link" to="/login">Logout</Link>
                        </li>
                    </Fragment>) : null}
                </ul>
            </div>
        </div>
    </nav>
    )
}

export { Navbar };