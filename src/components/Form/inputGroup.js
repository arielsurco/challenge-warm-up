const InputGroup = ({className, children}) => {
    return (
        <div className={`form__input-group ${className || ''}`}>
            {children}
        </div>
    )
}

export { InputGroup }