import './Form.css';

const Form = ({children, className, ...formProps}) => {
    return (
        <form {...formProps} className={`form ${className || ''}`}>
            {children}
        </form>
    )
}

export { Form }