const InputItem = ({name, label, type, placeholder, className, error, ...rest}) => {
    return (
        <div className={`input-item ${className || ''}`}>
            <label htmlFor={name}>{label}</label>
            <input type={type || 'text'} name={name} id={name} placeholder={placeholder || label} {...rest} />
            {error && <p className="input-item__error">{error}</p>}
        </div>
    )
}

export { InputItem }